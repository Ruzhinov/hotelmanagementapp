The project is using H2 embedded database, which can be accessed with this URL:
http://localhost:8080/hotel-manager/h2-console

There is Swagger that shows two API definitions - hotel-system and hotel-public:
http://localhost:8080/hotel-manager/swagger-ui/index.html

<b>TASK</b>

Imagine that you are tasked with creating the base Java code for a Hotel Management System.
The system has customer-facing features and administrative features.
The Hotel Management System has the following set of requirements for the customer-facing features:
1.	The system should support the booking of different room types like standard, deluxe, family suite, etc.
•	Each room type has furniture (chairs, beds, tables, lamps, wardrobes, etc.)
•	Each room type has amenities (e.g. bathrooms, toilet, etc.)
•	Each room type has appliances (TVs, air conditioners, Wi-Fi, etc.)
2.	Guests should be able to search rooms:
•	by room type
•	by room furniture
•	by room amenities
•	by room appliances
3.	The system should allow rooms to be booked by guests A booking should contain:
•	the personal details of the guest making the reservation (it is up to the candidate to decide what personal fields to include)
•	the total number of guests
•	the room name
•	the date of reservation
4.	The system should allow:
•	a customer to view their reservations
•	a reservation to be cancelled up to 24 hours after booking.
The Hotel Management System has the following set of requirements for the administrative features:
1.	The system should support the creation of rooms
2.	The system should support the searching of rooms, identical to the search capabilities provided to hotel guests via the customer-facing features
3.	The system should support the following operations on reservations:
a.	Cancelling a reservation (e.g. a customer cancels over the phone, instead of using the Hotel Management System) 
b.	Searching reservations
i.	by room type
ii.	by customer name
Assume that all data is stored in-memory using Java data structures.
