package com.vmware.example.HotelManager.model.inventory;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Objects;

@Entity
@DiscriminatorValue("Appliance")
public final class Appliance extends AbstractInventoryItem {

    public static enum Category {
        ENTERTAINMENT,
        SMART_HOME,
        OTHER;
    }

    private Category category;

    Appliance() {
        super();
    }

    public Appliance(final String type, final String name) {
        this(type, name, "");
    }

    public Appliance(final String type, final String name, final String description) {
        super(type, name, description);
        this.category = Category.OTHER;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "Appliance {" +
                "type='" + getType() + '\'' +
                ", name='" + getName() + '\'' +
                '}';
    }
}
