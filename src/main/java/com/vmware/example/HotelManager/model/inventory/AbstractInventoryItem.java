package com.vmware.example.HotelManager.model.inventory;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@DiscriminatorColumn(name="inherit")
public abstract class AbstractInventoryItem implements InventoryItem {

    @Id
    private String type;

    private String name;
    private String description;

    AbstractInventoryItem() {

    }

    public AbstractInventoryItem(final String type, final String name, final String description) {
        if (type == null || type.trim().isEmpty()) {
            throw new IllegalArgumentException("Type is required!");
        }
        this.type = type.trim();
        this.name = (name == null || name.trim().isEmpty() ? this.type : name);
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = (name == null || name.trim().isEmpty() ? this.type : name);
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AbstractInventoryItem item = (AbstractInventoryItem) o;
        return type.equals(item.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type);
    }

    @Override
    public String toString() {
        return "InventoryItem {" +
                "type='" + type + '\'' +
                ", name='" + name + '\'' +
                '}';
    }

}
