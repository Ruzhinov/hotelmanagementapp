package com.vmware.example.HotelManager.model.inventory;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.vmware.example.HotelManager.model.Room;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@DiscriminatorValue("RoomType")
public final class RoomType extends AbstractInventoryItem {

    @JsonIgnore
    @OneToMany(mappedBy = "type", fetch = FetchType.LAZY)
    private List<Room> rooms;

    private Integer maxGuests;

    RoomType() {
        super();
    }

    public RoomType(final String type, final String name) {
        this(type, name, "");
    }

    public RoomType(final String type, final String name, final String description) {
        super(type, name, description);
    }

    public List<Room> getRooms() {
        return rooms;
    }

    public void setRooms(List<Room> rooms) {
        this.rooms = rooms;
    }

    public Integer getMaxGuests() {
        return maxGuests;
    }

    public void setMaxGuests(Integer maxGuests) {
        this.maxGuests = maxGuests;
    }

    @Override
    public String toString() {
        return "RoomType {" +
                "type='" + getType() + '\'' +
                ", name='" + getName() + '\'' +
                '}';
    }
}
