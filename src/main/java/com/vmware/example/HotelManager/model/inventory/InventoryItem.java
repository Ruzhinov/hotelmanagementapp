package com.vmware.example.HotelManager.model.inventory;

/**
 * The interface Inventory item.
 */
public interface InventoryItem {

    /**
     * Gets type uniquely identify inventory item.
     *
     * @return the type
     */
    String getType();

    /**
     * Gets display name.
     *
     * @return the name
     */
    String getName();

    /**
     * Gets optional description.
     *
     * @return the description
     */
    String getDescription();

}
