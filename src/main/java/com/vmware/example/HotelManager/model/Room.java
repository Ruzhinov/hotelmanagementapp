package com.vmware.example.HotelManager.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.vmware.example.HotelManager.model.inventory.*;


import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Entity
public class Room {

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(nullable = false)
    private RoomType type;

    // Display short info
    private String name;
    private String description;
    private String view;

    // Location in the hotel
    @Column(nullable = false)
    private String doorNumber;
    @Column(nullable = false)
    private String blockNumber;

    // Room properties
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    private Set<Furniture> furniture;
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    private Set<Amenity> amenities;
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    private Set<Appliance> appliances;

    private Integer maxGuests;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    private Reservation reservation;

    Room() {
        super();
    }

    public Room(final RoomType type) {
        if (type == null) {
            throw new IllegalArgumentException("Type is required!");
        }
        this.type = type;
    }

    private Room(final Builder builder) {
        this.type = builder.type;
        this.name = builder.name;
        this.description = builder.description;
        this.view = builder.view;
        this.maxGuests = builder.maxGuests;
        this.doorNumber = builder.doorNumber;
        this.blockNumber = builder.blockNumber;
        this.furniture = builder.furniture;
        this.amenities = builder.amenities;
        this.appliances = builder.appliances;
    }

    public Long getId() {
        return id;
    }

    public RoomType getType() {
        return type;
    }

    public Reservation getReservation() {
        return reservation;
    }

    public void setReservation(Reservation reservation) {
        this.reservation = reservation;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getView() {
        return view;
    }

    public void setView(String view) {
        this.view = view;
    }

    public String getDoorNumber() {
        return doorNumber;
    }

    public void setDoorNumber(String doorNumber) {
        this.doorNumber = doorNumber;
    }

    public String getBlockNumber() {
        return blockNumber;
    }

    public void setBlockNumber(String blockNumber) {
        this.blockNumber = blockNumber;
    }

    public Set<Furniture> getFurniture() {
        return furniture;
    }

    public void setFurniture(Set<Furniture> furniture) {
        this.furniture = furniture;
    }

    public Set<Amenity> getAmenities() {
        return amenities;
    }

    public void setAmenities(Set<Amenity> amenities) {
        this.amenities = amenities;
    }

    public Set<Appliance> getAppliances() {
        return appliances;
    }

    public void setAppliances(Set<Appliance> appliances) {
        this.appliances = appliances;
    }

    public Integer getMaxGuests() {
        return maxGuests;
    }

    public void setMaxGuests(Integer maxGuests) {
        this.maxGuests = maxGuests;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Room room = (Room) o;
        return type.equals(room.type) && Objects.equals(doorNumber, room.doorNumber) && Objects.equals(blockNumber, room.blockNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, doorNumber, blockNumber);
    }

    @Override
    public String toString() {
        return "Room {" +
                "id=" + id +
                ", type=" + type +
                ", doorNumber='" + doorNumber + '\'' +
                ", blockNumber='" + blockNumber + '\'' +
                '}';
    }

    public static Builder getBuilder(RoomType type) {
        return new Builder(type);
    }

    public static class Builder {

        private final RoomType type;
        private String name;
        private String description;
        private String view;
        private String doorNumber;
        private String blockNumber;
        private Integer maxGuests;
        private final Set<Furniture> furniture = new HashSet<>();
        private final Set<Amenity> amenities = new HashSet<>();
        private final Set<Appliance> appliances = new HashSet<>();

        private Builder(RoomType type) {
            this.type = type;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder description(String description) {
            this.description = description;
            return this;
        }

        public Builder view(String view) {
            this.view = view;
            return this;
        }

        public Builder doorNumber(String doorNumber) {
            this.doorNumber = doorNumber;
            return this;
        }

        public Builder blockNumber(String blockNumber) {
            this.blockNumber = blockNumber;
            return this;
        }

        public Builder maxGuests(int maxGuests) {
            this.maxGuests = maxGuests;
            return this;
        }

        public Builder addFurniture(Furniture furniture) {
            this.furniture.add(furniture);
            return this;
        }

        public Builder addFurniture(Iterable<Furniture> furniture) {
            furniture.forEach(this.furniture::add);
            return this;
        }

        public Builder addAmenity(Amenity amenity) {
            this.amenities.add(amenity);
            return this;
        }

        public Builder addAmenities(Iterable<Amenity> amenity) {
            amenity.forEach(this.amenities::add);
            return this;
        }

        public Builder addAppliance(Appliance appliance) {
            this.appliances.add(appliance);
            return this;
        }

        public Builder addAppliances(Iterable<Appliance> appliance) {
            appliance.forEach(this.appliances::add);
            return this;
        }

        public Room build() {
            return new Room(this);
        }
    }
}
