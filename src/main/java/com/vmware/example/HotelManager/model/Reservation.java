package com.vmware.example.HotelManager.model;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
public class Reservation {

    public enum Status {
        UNKNOWN,
        CONFIRM,
        CANCELED,
        PAYED;
    }

    public enum GuestType {
        CHILDREN_3,
        CHILDREN_6,
        CHILDREN_10,
        CHILDREN_16,
        ADULTS
    }

    @Id
    @GeneratedValue
    private Long id;

    @OneToMany(fetch = FetchType.EAGER, cascade = { CascadeType.REFRESH }, mappedBy = "reservation")
    @Column(nullable = false)
    private Set<Room> rooms;

    @JsonIgnore
    @ManyToOne(optional = false, fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    private Customer customer;

    @Column(nullable = false)
    private Status status;

    @Column(nullable = false)
    private Date checkIn;

    @Column(nullable = false)
    private Date checkOut;

    @Column(nullable = false)
    private Date createdAt;

    @ElementCollection(fetch = FetchType.EAGER)
    @Column(nullable = false)
    private Map<GuestType, Integer> guests;

    Reservation() {
        this.status = Status.UNKNOWN;
    }

    private Reservation(final Builder builder) {
        this.status = builder.status;
        this.checkIn = builder.checkIn;
        this.checkOut = builder.checkOut;
        this.createdAt = builder.createdAt;
        this.guests = builder.guests;
        this.customer = builder.customer;
        this.rooms = builder.rooms;
    }

    public Long getId() {
        return id;
    }

    public Set<Room> getRooms() {
        return rooms;
    }

    public void setRooms(Set<Room> rooms) {
        this.rooms = rooms;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Date getCheckIn() {
        return checkIn;
    }

    public void setCheckIn(Date checkIn) {
        this.checkIn = checkIn;
    }

    public Date getCheckOut() {
        return checkOut;
    }

    public void setCheckOut(Date checkOut) {
        this.checkOut = checkOut;
    }

    public Map<GuestType, Integer> getGuests() {
        return guests;
    }

    public void setGuests(Map<GuestType, Integer> guests) {
        this.guests = guests;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public String toString() {
        return "Reservation {" +
                "id=" + id +
                ", rooms=" + rooms +
                ", customer=" + customer +
                ", status=" + status +
                ", checkIn=" + checkIn +
                ", checkOut=" + checkOut +
                ", guests=" + guests +
                '}';
    }

    public static Builder getBuilder() {
        return new Builder();
    }

    public static class Builder {

        private Status status;
        private Date checkIn;
        private Date checkOut;
        private Date createdAt;

        private Map<GuestType, Integer> guests;

        private Set<Room> rooms;

        private Customer customer;

        private Builder() {
            this.guests = new HashMap<>();
            this.rooms = new HashSet<>();
        }

        public Builder status(Status status) {
            this.status = status;
            return this;
        }

        public Builder checkIn(Date checkIn) {
            this.checkIn = checkIn;
            return this;
        }

        public Builder checkOut(Date checkOut) {
            this.checkOut = checkOut;
            return this;
        }

        public Builder createdAt(Date createdAt) {
            this.createdAt = createdAt;
            return this;
        }

        public Builder guests(final Map<GuestType, Integer> guests) {
            guests.forEach((type, number) -> {
                this.addGuest(type, number);
            });
            return this;
        }

        public Builder addGuest(GuestType guestType, int numberOfGuests) {
            Integer num = this.guests.get(guestType);
            if (num == null) {
                num = new Integer(0);
            }
            this.guests.put(guestType, num + numberOfGuests);
            return this;
        }

        public Builder customer(Customer customer) {
            this.customer = customer;
            return this;
        }

        public Builder addRoom(Room room) {
            this.rooms.add(room);
            return this;
        }

        public Builder addRooms(Iterable<Room> rooms) {
            rooms.forEach(this.rooms::add);
            return this;
        }

        public Reservation build() {
            return new Reservation(this);
        }
    }
}
