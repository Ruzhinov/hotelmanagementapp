package com.vmware.example.HotelManager.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import java.util.Map;

@JsonAutoDetect
public class Action {

    private String name;
    private Map<String, Object> inputs;

    public Action(String name) {
        this(name, null);
    }

    public Action(String name, Map<String, Object> inputs) {
        this.name = name;
        this.inputs = inputs;
    }

    public String getName() {
        return name;
    }

    public Map<String, Object> getInputs() {
        return inputs;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setInputs(Map<String, Object> inputs) {
        this.inputs = inputs;
    }
}
