package com.vmware.example.HotelManager.model.inventory;

import javax.persistence.Entity;
import javax.persistence.DiscriminatorValue;

@Entity
@DiscriminatorValue("Amenity")
public final class Amenity extends AbstractInventoryItem {

    private boolean outdoor;

    Amenity() {
        super();
    }

    public Amenity(final String type, final String name) {
        this(type, name, "");
    }

    public Amenity(final String type, final String name, final String description) {
        super(type, name, description);
    }

    public boolean isOutdoor() {
        return outdoor;
    }

    public void setOutdoor(boolean outdoor) {
        this.outdoor = outdoor;
    }

    @Override
    public String toString() {
        return "Amenity {" +
                "type='" + getType() + '\'' +
                ", name='" + getName() + '\'' +
                '}';
    }
}
