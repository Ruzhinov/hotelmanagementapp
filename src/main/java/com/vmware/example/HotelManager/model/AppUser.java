package com.vmware.example.HotelManager.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class AppUser {

    public enum Role {
        ADMIN,
        USER
    }

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private Role role;

    @Column(unique = true, nullable = false)
    private String email;

    @Column(nullable = false)
    private String password;

    @Column(nullable = false)
    private String firstName;
    @Column(nullable = false)
    private String lastName;
    @Column(nullable = false)
    private String phone;

    AppUser() {
        super();
    }

    public AppUser(final Role role, final String email, final String firstName, final String lastName, final String phone) {
        if (role == null) {
            throw new IllegalArgumentException("User role is required!");
        }
        if (email == null || email.trim().isEmpty()) {
            throw new IllegalArgumentException("User Email is required!");
        }
        if (firstName == null || firstName.trim().isEmpty()) {
            throw new IllegalArgumentException("User first name is required!");
        }
        if (lastName == null || lastName.trim().isEmpty()) {
            throw new IllegalArgumentException("User last name is required!");
        }
        if (phone == null || phone.trim().isEmpty()) {
            throw new IllegalArgumentException("User phone is required!");
        }

        this.role = role;
        this.email = email.trim();
        this.firstName = firstName.trim();
        this.lastName = lastName.trim();
        this.phone = phone.trim();
    }

    public Long getId() {
        return id;
    }

    public Role getRole() {
        return role;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AppUser customer = (AppUser) o;
        return email.equals(customer.email) && role.equals(customer.role);
    }

    @Override
    public int hashCode() {
        return Objects.hash(email, role);
    }

    @Override
    public String toString() {
        return "System User {" +
                "id=" + id +
                ", role='" + role + '\'' +
                ", email='" + email + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }
}
