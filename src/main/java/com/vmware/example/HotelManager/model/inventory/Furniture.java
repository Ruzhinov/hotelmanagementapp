package com.vmware.example.HotelManager.model.inventory;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Objects;

@Entity
@DiscriminatorValue("Furniture")
public final class Furniture extends AbstractInventoryItem {

    private boolean outdoor;

    Furniture() {
        super();
    }

    public Furniture(final String type, final String name) {
        this(type, name, "");
    }

    public Furniture(final String type, final String name, final String description) {
        super(type, name, description);
    }

    public boolean isOutdoor() {
        return outdoor;
    }

    public void setOutdoor(boolean outdoor) {
        this.outdoor = outdoor;
    }

    @Override
    public String toString() {
        return "Furniture {" +
                "type='" + getType() + '\'' +
                ", name='" + getName() + '\'' +
                '}';
    }
}
