package com.vmware.example.HotelManager;

import com.vmware.example.HotelManager.model.AppUser;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;

import java.util.stream.Stream;

@Component
public class AuthenticationContext implements IAuthenticationContext {

    @Override
    public Authentication getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    @Override
    public User getUser() {
        final Authentication auth = this.getAuthentication();
        if (auth.getPrincipal() instanceof User) {
            return (User) auth.getPrincipal();
        }
        return null;
    }

    @Override
    public boolean hasAuthorizationRole(AppUser.Role role) {
        final Authentication auth = this.getAuthentication();
//        System.out.println("AuthenticationContext: " + auth.getPrincipal());
        boolean isAuthorized = false;
        if (auth.getPrincipal() instanceof User) {
            final User loginUser = (User) auth.getPrincipal();
            if (hasAuthorities(loginUser, role)) {
                isAuthorized = true;
            }
        }

        return isAuthorized;
    }

    private static boolean hasAuthorities(User user, AppUser.Role role) {
        return user.getAuthorities().stream().filter(auth -> role.name().equals(auth.getAuthority())).count() != 0;
    }
}
