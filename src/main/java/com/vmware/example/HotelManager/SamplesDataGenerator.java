package com.vmware.example.HotelManager;

import com.vmware.example.HotelManager.controller.AmenitiesRepository;
import com.vmware.example.HotelManager.controller.AppUsersRepository;
import com.vmware.example.HotelManager.controller.AppliancesRepository;
import com.vmware.example.HotelManager.controller.CustomersRepository;
import com.vmware.example.HotelManager.controller.FurnitureRepository;
import com.vmware.example.HotelManager.controller.ReservationsRepository;
import com.vmware.example.HotelManager.controller.RoomTypeRepository;
import com.vmware.example.HotelManager.controller.RoomsRepository;
import com.vmware.example.HotelManager.model.AppUser;
import com.vmware.example.HotelManager.model.Customer;
import com.vmware.example.HotelManager.model.Reservation;
import com.vmware.example.HotelManager.model.Room;
import com.vmware.example.HotelManager.model.inventory.Amenity;
import com.vmware.example.HotelManager.model.inventory.Appliance;
import com.vmware.example.HotelManager.model.inventory.Furniture;
import com.vmware.example.HotelManager.model.inventory.RoomType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class SamplesDataGenerator {

    @Autowired
    AppUsersRepository appUsersRepo;

    @Autowired
    CustomersRepository customersRepo;

    @Autowired
    RoomsRepository roomsRepo;

    @Autowired
    RoomTypeRepository roomTypeRepository;
    @Autowired
    FurnitureRepository furnitureRepository;
    @Autowired
    AppliancesRepository appliancesRepository;
    @Autowired
    AmenitiesRepository amenitiesRepository;

    @Autowired
    ReservationsRepository reservationsRepo;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Bean
    public CommandLineRunner loadData() {
        return args -> {
            // add customers
            final List<Customer> testCustomers = new ArrayList<>();
            Arrays.asList("ivan@gmail.com Ivan Petkov",
                            "peter@gmail.com Peter Ivanov",
                            "stefan@gmail.com Stefan Georgiev",
                            "petia@gmail.com Petia Markova").stream()
                .forEach(nameInfo -> {
                    String[] userInfo = nameInfo.split(" ");
                    final Customer customer = new Customer(userInfo[0], userInfo[1], userInfo[2], "0888 123456");
                    customer.setInfo(nameInfo);
                    customer.setPassword(passwordEncoder.encode("guesta"));
                    final Customer stored = customersRepo.save(customer);
                    testCustomers.add(stored);
                });


            // add system user
            final AppUser admin = new AppUser(AppUser.Role.ADMIN, "admin", "Admin", "User", "0888 123456");
            admin.setPassword(passwordEncoder.encode("admina"));
            appUsersRepo.save(admin);
            final AppUser user = new AppUser(AppUser.Role.USER, "user", "User", "User", "0888 123456");
            user.setPassword(passwordEncoder.encode("useraa"));
            appUsersRepo.save(user);

            // add room types
            List<RoomType> roomTypes = new ArrayList<>();
            RoomType roomType = new RoomType("Standard", "Standard Room", "Standard Room two beds");
            roomType.setMaxGuests(2);
            roomTypeRepository.save(roomType);
            roomTypes.add(roomType);

            roomType = new RoomType("Deluxe", "Deluxe Room", "Deluxe Room with two + one beds");
            roomType.setMaxGuests(2+1);
            roomTypeRepository.save(roomType);
            roomTypes.add(roomType);

            roomType = new RoomType("Studio", "Studio", "Studio with two rooms");
            roomType.setMaxGuests(3+1);
            roomTypeRepository.save(roomType);
            roomTypes.add(roomType);

            roomType = new RoomType("Family", "Family Suite", "Family apartment");
            roomType.setMaxGuests(4+1);
            roomTypeRepository.save(roomType);
            roomTypes.add(roomType);

            // add furniture
            List<Furniture> furnitureList = Arrays.asList("Chair", "Table", "Lamp", "Wardrobe").stream().map(type -> {
                Furniture furniture = new Furniture(type, type, "Furniture Description - " + type);
                furniture.setOutdoor(false);
                return furniture;
            }).collect(Collectors.toList());
            furnitureList.forEach(furniture -> {
                furnitureRepository.save(furniture);
            });

            // add appliances
            List<Appliance> applianceList = Arrays.asList("TV", "Air Conditioner", "Wi-Fi", "Oven").stream().map(type -> {
                Appliance appliance = new Appliance(type, type, "Appliance Description - " + type);
                appliance.setCategory(Appliance.Category.OTHER);
                return appliance;
            }).collect(Collectors.toList());
            applianceList.forEach(appliance -> {
                appliancesRepository.save(appliance);
            });

            // add amenities
            List<Amenity> amenityList = Arrays.asList("Bathroom", "Toilet", "Balcony").stream().map(type -> {
                Amenity amenity = new Amenity(type, type, "Amenity Description - " + type);
                return amenity;
            }).collect(Collectors.toList());
            amenityList.forEach(amenity -> {
                amenitiesRepository.save(amenity);
            });

            // add rooms
            final List<String> doorNumbers = Arrays.asList("1A", "2A", "3A", "3A-1", "3A-2", "3B", "4B", "5B");
            final List<String> blockNumbers = Arrays.asList("Block 1", "Block 2");
            final List<Room> testRooms = new ArrayList<>();
            roomTypes.forEach(type -> {
                blockNumbers.forEach(blockNumber -> {
                    doorNumbers.forEach(doorNumber -> {
                        final Room room = Room.getBuilder(type)
                                .name(type.getName() + " " + doorNumber)
                                .description(type.getDescription())
                                .view("Sea View")
                                .doorNumber(doorNumber)
                                .blockNumber(blockNumber)
                                .maxGuests(type.getMaxGuests() + 1)
                                .addAmenity(amenityList.get(new Random().nextInt(amenityList.size())))
                                .addAmenity(amenityList.get(new Random().nextInt(amenityList.size())))
                                .addAppliance(applianceList.get(new Random().nextInt(applianceList.size())))
                                .addAppliance(applianceList.get(new Random().nextInt(applianceList.size())))
                                .addFurniture(furnitureList.get(new Random().nextInt(furnitureList.size())))
                                .addFurniture(furnitureList.get(new Random().nextInt(furnitureList.size())))
                                .addFurniture(furnitureList.get(new Random().nextInt(furnitureList.size())))
                                .build();
                        testRooms.add(roomsRepo.save(room));
                    });
                });
            });

            testRooms.subList(0, 5).forEach(room -> {
                final Calendar checkIn = Calendar.getInstance();
                checkIn.setTime(new Date());
                checkIn.add(Calendar.DATE, 1);
                final Calendar checkOut = Calendar.getInstance();
                checkOut.setTime(new Date());
                checkOut.add(Calendar.DATE, 5);
                final Reservation newReservation = Reservation.getBuilder()
                        .status(Reservation.Status.UNKNOWN)
                        .createdAt(new Date())
                        .checkIn(checkIn.getTime())
                        .checkOut(checkOut.getTime())
                        .guests(Collections.singletonMap(Reservation.GuestType.ADULTS, 2))
                        .customer(testCustomers.get(0))
                        .addRoom(room)
                        .build();
                testCustomers.get(0).addReservation(newReservation);
                room.setReservation(reservationsRepo.save(newReservation));
                roomsRepo.save(room);
            });

        };
    }

}
