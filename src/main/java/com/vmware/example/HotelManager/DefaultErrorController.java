package com.vmware.example.HotelManager;

import org.springframework.boot.autoconfigure.web.servlet.error.AbstractErrorController;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.Map;

//@Controller
//@RequestMapping({"/error"})
public class DefaultErrorController extends AbstractErrorController {

    public DefaultErrorController(final ErrorAttributes errorAttributes) {
        super(errorAttributes, Collections.emptyList());
    }

    @RequestMapping
    public String handleError(Model model, HttpServletRequest request) {
        Map<String, Object> errorAttributes = this.getErrorAttributes(request,
                ErrorAttributeOptions.of(ErrorAttributeOptions.Include.MESSAGE, ErrorAttributeOptions.Include.EXCEPTION));
        // retrieve the default error attributes as a key/value map (timestamp, status, error...)
        System.err.println("Error: " + errorAttributes);
        return "error";
    }

}