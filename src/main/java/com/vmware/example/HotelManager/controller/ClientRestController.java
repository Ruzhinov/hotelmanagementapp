package com.vmware.example.HotelManager.controller;

import com.vmware.example.HotelManager.IAuthenticationContext;
import com.vmware.example.HotelManager.model.Action;
import com.vmware.example.HotelManager.model.AppUser;
import com.vmware.example.HotelManager.model.Customer;
import com.vmware.example.HotelManager.model.Reservation;
import com.vmware.example.HotelManager.model.Room;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AuthorizationServiceException;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1/client")
public class ClientRestController {

    private static final Logger logger = LoggerFactory.getLogger(ClientRestController.class);

    @Autowired
    protected IAuthenticationContext authContext;

    @Autowired
    protected CustomersRepository customersRepo;

    @Autowired
    protected RoomsRepository roomsRepo;

    @Autowired
    protected ReservationsRepository reservationsRepo;

    @GetMapping("/health")
    public String health() {
        return "OK";
    }

    @PostMapping(path = "/user", consumes = "application/json", produces = "application/json")
    public Customer registerCustomer(@RequestBody Customer request) {
        final Customer newCustomer = new Customer(request.getEmail(), request.getFirstName(), request.getLastName(),
                request.getPhone());
        final String password = request.getPassword();
        if (password == null || password.length() < 6) {
            throw new IllegalArgumentException("Password can not be empty and must have at least 6 characters!");
        }
        newCustomer.setPassword(password);
        newCustomer.setInfo(request.getInfo());
        Customer customerResult = customersRepo.save(newCustomer);
        customerResult.setPassword("******");
        return customerResult;
    }

    @GetMapping(path = "/user/{id}", produces = "application/json")
    public Customer getCustomer(@PathVariable Long id) {
        Optional<Customer> result = customersRepo.findById(id);
        if (!result.isPresent()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Unable to find customer!");
        }
        final Customer customerResult = result.get();
        customerResult.setPassword("******");
        return customerResult;
    }

    @GetMapping(path = "/user/{customerId}/reservation", produces = "application/json")
    public Set<Reservation> getCustomerReservations(@PathVariable Long customerId) {
        Optional<Customer> result = customersRepo.findById(customerId);
        if (!result.isPresent()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Unable to find customer!");
        }
        final Customer customerResult = result.get();
        return customerResult.getReservations();
    }

    @GetMapping(path = "/user/{customerId}/reservation/{reservationId}", produces = "application/json")
    public Reservation getCustomerReservation(@PathVariable Long customerId, @PathVariable Long reservationId) {
        Optional<Customer> result = customersRepo.findById(customerId);
        if (!result.isPresent()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Unable to find customer!");
        }
        Optional<Reservation> reservation = reservationsRepo.findById(reservationId);
        if (!reservation.isPresent()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Unable to find reservation!");
        }
        return reservation.get();
    }

    @PostMapping(path = "/user/{customerId}/reservation/{reservationId}/action", consumes = "application/json")
    public void reservationAction(@PathVariable Long customerId, @PathVariable Long reservationId, @RequestBody Action action) {
        final User loginUser = authContext.getUser();
        final Customer customer = customersRepo.findByEmail(loginUser.getUsername());
        if (customer == null || !customer.getId().equals(customerId)) {
            throw new AuthorizationServiceException("Unauthorized access!");
        }

        action.setInputs(Collections.singletonMap("customerId", customerId));
        SystemRestController.doReservationAction(reservationId, action, false, customersRepo, reservationsRepo, roomsRepo);
    }

    @GetMapping(path = "/room", produces = "application/json")
    public Set<Room> getRooms(@RequestParam(required = false) String filterType,
                              @RequestParam(required = false) String filterFurniture,
                              @RequestParam(required = false) String filterAmenities,
                              @RequestParam(required = false) String filterAppliances,
                              @RequestParam(required = false) String keywords) {
        Set<Room> rooms;
        if ((filterType != null && !filterType.isEmpty()) &&
                (filterFurniture != null && !filterFurniture.isEmpty()) &&
                (filterAmenities != null && !filterAmenities.isEmpty()) &&
                (filterAppliances != null && !filterAppliances.isEmpty())) {

            rooms = roomsRepo.findByType_TypeIgnoreCaseAndFurniture_TypeIgnoreCaseAndAmenities_TypeIgnoreCaseAndAppliances_TypeIgnoreCase(
                    filterType, filterFurniture, filterAmenities, filterAppliances);
        } else if ((filterType != null && !filterType.isEmpty()) &&
                    (filterFurniture != null && !filterFurniture.isEmpty()) &&
                    (filterAmenities != null && !filterAmenities.isEmpty())) {
            rooms = roomsRepo.findByType_TypeIgnoreCaseAndFurniture_TypeIgnoreCaseAndAmenities_TypeIgnoreCase(
                    filterType, filterFurniture, filterAmenities);
        } else if ((filterType != null && !filterType.isEmpty()) &&
                    (filterFurniture != null && !filterFurniture.isEmpty()) &&
                    (filterAppliances != null && !filterAppliances.isEmpty())) {
            rooms = roomsRepo.findByType_TypeIgnoreCaseAndFurniture_TypeIgnoreCaseAndAppliances_TypeIgnoreCase(
                    filterType, filterFurniture, filterAppliances);
        } else if ((filterType != null && !filterType.isEmpty()) &&
                    (filterAmenities != null && !filterAmenities.isEmpty()) &&
                    (filterAppliances != null && !filterAppliances.isEmpty())) {
            rooms = roomsRepo.findByType_TypeIgnoreCaseAndAmenities_TypeIgnoreCaseAndAppliances_TypeIgnoreCase(
                    filterType, filterAmenities, filterAppliances);
        } else if ((filterType != null && !filterType.isEmpty()) &&
                    (filterFurniture != null && !filterFurniture.isEmpty())) {
            rooms = roomsRepo.findByType_TypeIgnoreCaseAndFurniture_TypeIgnoreCase(
                    filterType, filterFurniture);
        } else if ((filterType != null && !filterType.isEmpty()) &&
                    (filterAmenities != null && !filterAmenities.isEmpty())) {
            rooms = roomsRepo.findByType_TypeIgnoreCaseAndAmenities_TypeIgnoreCase(
                    filterType, filterAmenities);
        } else if ((filterType != null && !filterType.isEmpty()) &&
                    (filterAppliances != null && !filterAppliances.isEmpty())) {
            rooms = roomsRepo.findByType_TypeIgnoreCaseAndAppliances_TypeIgnoreCase(
                    filterType, filterAppliances);
        } else if (filterType != null && !filterType.isEmpty()) {
            rooms = roomsRepo.findByType_TypeIgnoreCase(filterType);
        } else {
            rooms = new HashSet<>();
            roomsRepo.findAll().forEach(rooms::add);
        }

        if (keywords != null && !keywords.trim().isEmpty()) {
            final Set<String> keywordsList = Arrays.asList(keywords.split(" ")).stream().map(keyword ->
                    keyword.toLowerCase()).collect(Collectors.toSet());
            rooms = rooms.stream().filter(room -> {
                List<String> roomTextCollection = new ArrayList<>();
                roomTextCollection.add(room.getType().getType());
                roomTextCollection.add(room.getName());
                roomTextCollection.add(room.getDescription());
                roomTextCollection.add(room.getView());
                roomTextCollection.addAll(room.getFurniture().stream().map(item -> item.getName()).collect(Collectors.toList()));
                roomTextCollection.addAll(room.getAmenities().stream().map(item -> item.getName()).collect(Collectors.toList()));
                roomTextCollection.addAll(room.getAppliances().stream().map(item -> item.getName()).collect(Collectors.toList()));
                final String roomText = String.join(" ", roomTextCollection).toLowerCase();
                return keywordsList.stream().allMatch(keyword -> roomText.indexOf(keyword) != -1);
            }).collect(Collectors.toSet());
        }

        return rooms;
    }

}
