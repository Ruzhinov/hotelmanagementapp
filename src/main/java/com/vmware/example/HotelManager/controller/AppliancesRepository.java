package com.vmware.example.HotelManager.controller;

import com.vmware.example.HotelManager.model.inventory.Appliance;
import com.vmware.example.HotelManager.model.inventory.RoomType;
import org.springframework.data.repository.CrudRepository;

public interface AppliancesRepository extends CrudRepository<Appliance, String> {

}
