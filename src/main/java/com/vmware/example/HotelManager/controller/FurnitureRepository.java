package com.vmware.example.HotelManager.controller;

import com.vmware.example.HotelManager.model.inventory.Furniture;
import com.vmware.example.HotelManager.model.inventory.RoomType;
import org.springframework.data.repository.CrudRepository;

public interface FurnitureRepository extends CrudRepository<Furniture, String> {

}
