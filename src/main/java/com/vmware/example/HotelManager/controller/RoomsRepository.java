package com.vmware.example.HotelManager.controller;

import com.vmware.example.HotelManager.model.Room;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Set;

public interface RoomsRepository extends CrudRepository<Room, Long> {

    Set<Room> findByType_TypeIgnoreCase(String type);

    Set<Room> findByType_TypeIgnoreCaseAndFurniture_TypeIgnoreCase(
            String type, String furnitureType);

    Set<Room> findByType_TypeIgnoreCaseAndFurniture_TypeIgnoreCaseAndAmenities_TypeIgnoreCase(
            String type, String furnitureType, String amenitiesType);

    Set<Room> findByType_TypeIgnoreCaseAndFurniture_TypeIgnoreCaseAndAppliances_TypeIgnoreCase(
            String type, String furnitureType, String appliancesType);

    Set<Room> findByType_TypeIgnoreCaseAndFurniture_TypeIgnoreCaseAndAmenities_TypeIgnoreCaseAndAppliances_TypeIgnoreCase(
            String type, String furnitureType, String amenitiesType, String appliancesType);

    Set<Room> findByType_TypeIgnoreCaseAndAmenities_TypeIgnoreCase(
            String type, String amenitiesType);

    Set<Room> findByType_TypeIgnoreCaseAndAmenities_TypeIgnoreCaseAndAppliances_TypeIgnoreCase(
            String type, String amenitiesType, String appliancesType);

    Set<Room> findByType_TypeIgnoreCaseAndAppliances_TypeIgnoreCase(
            String type, String appliancesType);



//    @Query("Select r from Room r where r.type.type = :roomType")
//    Set<Room> findRooms(@Param("roomType") String roomType);
//
//    @Query("Select r from Room r where r.type.type = :roomType and :furniture in r.furniture and :amenities in r.amenities and :appliances in r.appliances")
//    Set<Room> findRooms(@Param("roomType") String roomType,
//                        @Param("furniture") String furniture,
//                        @Param("amenities") String amenities,
//                        @Param("appliances") String appliances);

}
