package com.vmware.example.HotelManager.controller;

import com.vmware.example.HotelManager.model.AppUser;
import com.vmware.example.HotelManager.model.Customer;
import org.springframework.data.repository.CrudRepository;

public interface AppUsersRepository extends CrudRepository<AppUser, Long> {

    AppUser findByEmail(String email);
}
