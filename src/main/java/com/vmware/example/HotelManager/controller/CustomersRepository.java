package com.vmware.example.HotelManager.controller;

import com.vmware.example.HotelManager.model.Customer;
import org.springframework.data.repository.CrudRepository;

public interface CustomersRepository extends CrudRepository<Customer, Long> {

    Customer findByEmail(String email);

}
