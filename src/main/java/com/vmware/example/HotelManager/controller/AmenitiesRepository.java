package com.vmware.example.HotelManager.controller;

import com.vmware.example.HotelManager.model.inventory.Amenity;
import com.vmware.example.HotelManager.model.inventory.RoomType;
import org.springframework.data.repository.CrudRepository;

public interface AmenitiesRepository extends CrudRepository<Amenity, String> {

}
