package com.vmware.example.HotelManager.controller;

import com.vmware.example.HotelManager.IAuthenticationContext;
import com.vmware.example.HotelManager.model.Action;
import com.vmware.example.HotelManager.model.AppUser;

import com.vmware.example.HotelManager.model.Customer;
import com.vmware.example.HotelManager.model.Reservation;
import com.vmware.example.HotelManager.model.Room;
import com.vmware.example.HotelManager.model.inventory.Amenity;
import com.vmware.example.HotelManager.model.inventory.Appliance;
import com.vmware.example.HotelManager.model.inventory.Furniture;
import com.vmware.example.HotelManager.model.inventory.RoomType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AuthorizationServiceException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1/system")
public class SystemRestController {
    private static final Logger logger = LoggerFactory.getLogger(SystemRestController.class);

    @Autowired
    IAuthenticationContext authContext;

    @Autowired
    protected AppUsersRepository usersRepo;

    @Autowired
    protected RoomsRepository roomsRepo;

    @Autowired
    protected ReservationsRepository reservationsRepo;

    @Autowired
    RoomTypeRepository roomTypeRepository;
    @Autowired
    FurnitureRepository furnitureRepository;
    @Autowired
    AppliancesRepository appliancesRepository;
    @Autowired
    AmenitiesRepository amenitiesRepository;

    @Autowired
    protected CustomersRepository customersRepo;

    @GetMapping("/health")
    public String health() {
        return "OK";
    }

    @PostMapping(path = "/user", consumes = "application/json", produces = "application/json")
    public AppUser registerUser(@RequestBody AppUser request) {
        final AppUser newUser = new AppUser(request.getRole(), request.getEmail(), request.getFirstName(), request.getLastName(),
                request.getPhone());
        final String password = request.getPassword();
        if (password == null || password.length() < 6) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Password can not be empty and must have at least 6 characters!");
        }
        newUser.setPassword(password);
        final AppUser userResult = usersRepo.save(newUser);
        userResult.setPassword("******");
        return userResult;
    }

    @GetMapping(path = "/room/type", produces = "application/json")
    public List<RoomType> getRoomTypes() {
        List<RoomType> result = new ArrayList<>();
        roomTypeRepository.findAll().forEach(result::add);
        return result;
    }

    @GetMapping(path = "/room/type/{id}", produces = "application/json")
    public RoomType getRoomTypeById(@PathVariable String id) {
        Optional<RoomType> roomResult = roomTypeRepository.findById(id);
        if (!roomResult.isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Unable to find room of type " + id);
        }
        return roomResult.get();
    }

    @GetMapping(path = "/furniture", produces = "application/json")
    public List<Furniture> getFurniture() {
        List<Furniture> result = new ArrayList<>();
        furnitureRepository.findAll().forEach(result::add);
        return result;
    }

    @GetMapping(path = "/appliance", produces = "application/json")
    public List<Appliance> getAppliances() {
        List<Appliance> result = new ArrayList<>();
        appliancesRepository.findAll().forEach(result::add);
        return result;
    }

    @GetMapping(path = "/amenity", produces = "application/json")
    public List<Amenity> getAmenities() {
        List<Amenity> result = new ArrayList<>();
        amenitiesRepository.findAll().forEach(result::add);
        return result;
    }

    @GetMapping(path = "/room", produces = "application/json")
    public List<Room> getRooms(@RequestParam(required = false) String filterType) {
        final List<Room> rooms = new ArrayList<>();
        if (filterType != null && !filterType.trim().isEmpty()) {
            roomsRepo.findByType_TypeIgnoreCase(filterType).forEach(rooms::add);
        } else {
            roomsRepo.findAll().forEach(rooms::add);
        }
        return rooms;
    }

    @PostMapping(path = "/room", consumes = "application/json", produces = "application/json")
    public Room addRoom(@RequestBody Room request) {
        final Set<Furniture> requestFurniture = request.getFurniture();
        final List<Furniture> furnitureList = getFurniture().stream()
                .filter(item -> requestFurniture.contains(item)).collect(Collectors.toList());
        if (requestFurniture.size() != furnitureList.size()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Wrong furniture list!");
        }

        final Set<Appliance> requestAppliances = request.getAppliances();
        final List<Appliance> applianceList = getAppliances().stream().
                filter(item -> requestAppliances.contains(item)).collect(Collectors.toList());
        if (requestAppliances.size() != applianceList.size()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Wrong appliance list!");
        }

        final Set<Amenity> requestAmenities = request.getAmenities();
        final List<Amenity> amenityList = getAmenities().stream().
                filter(item -> requestAmenities.contains(item)).collect(Collectors.toList());
        if (requestAmenities.size() != amenityList.size()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Wrong amenity list!");
        }

        final RoomType roomType = getRoomTypeById(request.getType().getType());
        if (roomType == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, String.format("There is no room type %s!", request.getType().getType()));
        }

        final Room newRoom = Room.getBuilder(roomType)
                .name(roomType.getName() + " " + request.getDoorNumber())
                .description(roomType.getDescription())
                .view("Sea View")
                .doorNumber(request.getDoorNumber())
                .blockNumber(request.getBlockNumber())
                .maxGuests((roomType.getMaxGuests() != null ? roomType.getMaxGuests() : -1) + 1)
                .addAmenities(amenityList)
                .addAppliances(applianceList)
                .addFurniture(furnitureList)
                .build();

        return roomsRepo.save(newRoom);
    }

    /**
     * Add reservation.
     *
     * {
     *   "rooms": [
     *     {
     *       "type": {
     *         "type": "Standard"
     *       },
     *       "doorNumber": "3A",
     *       "blockNumber": "Block 1"
     *     }
     *   ],
     *   "customer": {
     *     "email": "ivan@gmail.com"
     *   },
     *   "checkIn": "2023-03-26T18:48:29.133Z",
     *   "checkOut": "2023-03-26T18:48:29.133Z",
     *   "guests": {
     *     "CHILDREN_6": 1,
     *     "ADULTS": 2
     *   }
     * }
     *
     * @param request the request
     * @return the reservation
     */
    @PostMapping(path = "/reservation/{customerId}", consumes = "application/json", produces = "application/json")
    public Reservation addReservation(@PathVariable Long customerId, @RequestBody Reservation request) {
        return addReservation(customerId, request, getRooms(null), customersRepo, reservationsRepo, roomsRepo);
    }

    private static synchronized Reservation addReservation(final Long customerId, final Reservation request,
                                                           final List<Room> allRooms,
                                                           final CustomersRepository customersRepo,
                                                           final ReservationsRepository reservationsRepo,
                                                           final RoomsRepository roomsRepo) {
        final Set<Room> requestRooms = request.getRooms();
        final List<Room> roomsList = allRooms.stream()
                .filter(item -> requestRooms.contains(item)).collect(Collectors.toList());
        if (requestRooms.size() != roomsList.size()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Wrong rooms list!");
        }
        if (request.getCheckIn().after(request.getCheckOut())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Wrong reservation dates!");
        }
        final int requestGuestsNumber = request.getGuests().values().stream().reduce(0, Integer::sum);
        roomsList.forEach(room -> {
            final Reservation roomReservation = room.getReservation();
            if (roomReservation != null) {
                final boolean freeBefore = request.getCheckOut().before(roomReservation.getCheckIn());
                final boolean freeAfter = request.getCheckIn().after(roomReservation.getCheckOut());
                if (!freeBefore && !freeAfter) {
                    throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Room '" + room.getName() + "' is reserved!");
                }
            }
            if (room.getMaxGuests() != null && requestGuestsNumber > room.getMaxGuests()) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Room '" + room.getName() +
                        "' guests number can not be more than " + room.getMaxGuests() + "!");
            }
        });

        final Optional<Customer> customerResult = customersRepo.findById(customerId);
        if (!customerResult.isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, String.format("There is no customer with email %s!",
                    request.getCustomer().getEmail()));
        }
        final Customer customer = customerResult.get();

        final Reservation newReservation = Reservation.getBuilder()
                .status(Reservation.Status.UNKNOWN)
                .checkIn(request.getCheckIn())
                .checkOut(request.getCheckOut())
                .createdAt(new Date())
                .guests(request.getGuests())
                .customer(customer)
                .addRooms(roomsList)
                .build();
        customer.addReservation(newReservation);
        final Reservation reservation = reservationsRepo.save(newReservation);

        roomsList.forEach(room -> {
            room.setReservation(reservation);
            roomsRepo.save(room);
        });
        return reservation;
    }

//    @Secured({"ADMIN", "USER"})
//    @PutMapping(path = "/reservation/{reservationId}", consumes = "application/json", produces = "application/json")
//    public Reservation updateReservation(@PathVariable Long reservationId, @RequestBody Reservation request) {
//        return updateReservation();
//    }

    @PostMapping(path = "/reservation/{reservationId}/action", consumes = "application/json")
    public void reservationAction(@PathVariable Long reservationId, @RequestBody Action action) {
        boolean isAuthorized = authContext.hasAuthorizationRole(AppUser.Role.ADMIN) ||
                authContext.hasAuthorizationRole(AppUser.Role.USER);
        if (!isAuthorized) {
            throw new AuthorizationServiceException("Unauthorized access!");
        }

        SystemRestController.doReservationAction(reservationId, action, true, customersRepo, reservationsRepo, roomsRepo);
    }

    static void doReservationAction(final Long reservationId, final Action action, final boolean adminRole,
                                    final CustomersRepository customersRepo,
                                    final ReservationsRepository reservationsRepo,
                                    final RoomsRepository roomsRepo) {
        if (action.getName() != null && "cancel".equals(action.getName().toLowerCase())) {
            final Object customerId = action.getInputs() != null ? action.getInputs().get("customerId") : null;
            if (customerId == null || !(customerId instanceof Number)) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                        "Missing parameter 'customerId' in action " + action.getName());
            }

            SystemRestController.cancelReservation(((Number)customerId).longValue(), reservationId, adminRole,
                    customersRepo, reservationsRepo, roomsRepo);
        } else {
            throw new UnsupportedOperationException("Unsupported action " + action.getName());
        }

    }

    private static synchronized Reservation cancelReservation(final Long customerId, final Long reservationId,
                                                              final boolean adminRole,
                                                              final CustomersRepository customersRepo,
                                                              final ReservationsRepository reservationsRepo,
                                                              final RoomsRepository roomsRepo) {
        Optional<Customer> result = customersRepo.findById(customerId);
        if (!result.isPresent()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Unable to find customer!");
        }
        Optional<Reservation> reservationsResult = reservationsRepo.findById(reservationId);
        if (!reservationsResult.isPresent()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Unable to find reservation!");
        }

        final Reservation reservation = reservationsResult.get();
        if (!reservation.getCustomer().getId().equals(customerId)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Unable to find reservation!");
        }

        if (!adminRole) {
            final Calendar tomorrow = Calendar.getInstance();
            tomorrow.setTime(new Date());
            tomorrow.add(Calendar.DATE, 1);
            if (reservation.getCreatedAt().after(tomorrow.getTime())) {
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Unable to cancel reservation!");
            }
        }

        reservation.setStatus(Reservation.Status.CANCELED);
        reservation.getRooms().forEach(room -> {
            room.setReservation(null);
            roomsRepo.save(room);
        });
        return reservationsRepo.save(reservation);
    }

    @GetMapping(path = "/reservation", produces = "application/json")
    public Set<Reservation> getReservation(@RequestParam("filterCustomer") String emailOrLastName,
                                           @RequestParam("filterRoomType") String roomType) {
        Set<Reservation> reservations;
        if (emailOrLastName != null && !emailOrLastName.trim().isEmpty()) {
            final String filter = emailOrLastName.trim().toLowerCase();
            reservations = reservationsRepo.findByCustomer_EmailOrCustomer_LastName(filter, filter);
            if (roomType != null && !roomType.trim().isEmpty()) {
                final String roomFilter = roomType.trim().toLowerCase();
                reservations = reservations.stream().filter(reservation ->
                        reservation.getRooms().stream().anyMatch(room ->
                                room.getType().getType().toLowerCase().equals(roomFilter))).collect(Collectors.toSet());
            }
        }else if (roomType != null && !roomType.trim().isEmpty()) {
            reservations = reservationsRepo.findByRooms_TypeContains(roomType.trim());
        } else {
            reservations = reservationsRepo.findAll();
        }

        return reservations;
    }

    @GetMapping(path = "/reservation/{reservationId}", produces = "application/json")
    public Optional<Reservation> getReservation(@PathVariable Long reservationId) {
        return reservationsRepo.findById(reservationId);
    }
}
