package com.vmware.example.HotelManager.controller;

import com.vmware.example.HotelManager.model.Reservation;
import org.springframework.data.repository.CrudRepository;

import java.util.Set;

public interface ReservationsRepository extends CrudRepository<Reservation, Long> {

    Reservation findByCustomer_Id(Long customerId);

    Set<Reservation> findByCustomer_EmailOrCustomer_LastName(String email, String customerLastName);

    Set<Reservation> findByRooms_TypeContains(String roomType);

    @Override
    Set<Reservation> findAll();
}
