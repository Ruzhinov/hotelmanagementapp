package com.vmware.example.HotelManager.controller;

import com.vmware.example.HotelManager.model.Room;
import com.vmware.example.HotelManager.model.inventory.RoomType;
import org.springframework.data.repository.CrudRepository;

public interface RoomTypeRepository extends CrudRepository<RoomType, String> {

}
