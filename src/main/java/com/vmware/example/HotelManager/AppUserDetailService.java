package com.vmware.example.HotelManager;

import com.vmware.example.HotelManager.controller.AppUsersRepository;
import com.vmware.example.HotelManager.controller.CustomersRepository;
import com.vmware.example.HotelManager.model.AppUser;
import com.vmware.example.HotelManager.model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class AppUserDetailService implements UserDetailsService {

    @Autowired
    private AppUsersRepository usersRepository;

    @Autowired
    private CustomersRepository customersRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        final AppUser user = usersRepository.findByEmail(username);
        System.out.println("AppUser Details >> " + username + " - " + user);
        if (user != null) {
            UserDetails userDetails = User.
                    withUsername(user.getEmail()).
                    password(user.getPassword()).authorities(user.getRole().name()).build();
            return userDetails;
        } else {
            final Customer customer = customersRepository.findByEmail(username);
            System.out.println("Customer Details >> " + username + " - " + customer);
            if (customer != null) {
                UserDetails userDetails = User.
                        withUsername(customer.getEmail()).
                        password(customer.getPassword()).authorities("CUSTOMER").build();
                return userDetails;
            } else {
                throw new UsernameNotFoundException(username);
            }
        }

    }
}
