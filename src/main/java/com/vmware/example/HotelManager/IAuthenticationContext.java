package com.vmware.example.HotelManager;

import com.vmware.example.HotelManager.model.AppUser;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;

public interface IAuthenticationContext {

    Authentication getAuthentication();

    User getUser();

    boolean hasAuthorizationRole(AppUser.Role role);

}
